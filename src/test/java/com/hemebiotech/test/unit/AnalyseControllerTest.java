package com.hemebiotech.test.unit;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;

import com.hemebiotech.analytics.controller.AnalyticsController;
import com.hemebiotech.analytics.file.impl.ReaderSymptomDataFromFile;
import com.hemebiotech.test.contract.IFileConfiguration;

import org.junit.jupiter.api.Test;

public class AnalyseControllerTest implements IFileConfiguration {

    @Test
    public void analyseSampleFromFile() throws URISyntaxException
    {
        AnalyticsController controller = new AnalyticsController();
        controller.analyse(new ReaderSymptomDataFromFile(getFilePath()));

        assertTrue(controller.getResults().size() > 0);

    }

}