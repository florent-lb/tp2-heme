package com.hemebiotech.test.unit;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.util.List;

import com.hemebiotech.analytics.entity.Symptom;
import com.hemebiotech.analytics.file.contract.ISymptomReader;
import com.hemebiotech.analytics.file.impl.ReaderSymptomDataFromFile;
import com.hemebiotech.test.contract.IFileConfiguration;

import org.junit.jupiter.api.Test;

public class ReadFileTest implements IFileConfiguration {

    /**
     * Read the file in path define in {@link #beforeTests}, test if line exist in
     * file and if reading succeed
     * 
     * @throws URISyntaxException
     */
    @Test
    public void lireFichier() throws URISyntaxException {
                          
        ISymptomReader reader = new ReaderSymptomDataFromFile(getFilePath());     
        List<Symptom> symptoms = reader.getSymptoms();
        assertNotNull(symptoms);        
        assertTrue(() -> symptoms.size() > 0);
    }

    

}
