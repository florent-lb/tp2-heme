package com.hemebiotech.test.unit;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.net.URISyntaxException;

import com.hemebiotech.analytics.controller.AnalyticsController;
import com.hemebiotech.analytics.file.impl.ReaderSymptomDataFromFile;
import com.hemebiotech.analytics.file.impl.WriterSymptomDataFile;
import com.hemebiotech.test.contract.IFileConfiguration;

import org.junit.jupiter.api.Test;

public class ExportFileResultTest implements IFileConfiguration {

    @Test
    public void write_the_result_of_file() throws URISyntaxException
    {
        AnalyticsController controller = new AnalyticsController();
        controller.analyse(new ReaderSymptomDataFromFile(getFilePath()));
        controller.exportResult(new WriterSymptomDataFile());
        
        File file = new File("result.out");

        assertTrue(file.exists());
    }
}