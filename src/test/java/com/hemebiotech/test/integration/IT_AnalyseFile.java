package com.hemebiotech.test.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.hemebiotech.analytics.controller.AnalyticsController;
import com.hemebiotech.analytics.file.impl.ReaderSymptomDataFromFile;
import com.hemebiotech.analytics.file.impl.WriterSymptomDataFile;
import com.hemebiotech.test.contract.IFileConfiguration;

import org.junit.jupiter.api.Test;

public class IT_AnalyseFile implements IFileConfiguration {

    @Test
    public void analyse_a_file() throws URISyntaxException, IOException
    {
        AnalyticsController controller = new AnalyticsController();

        controller.analyse(new ReaderSymptomDataFromFile(getFilePath()));

        controller.exportResult(new WriterSymptomDataFile());

        List<String> lines = Files.readAllLines(Paths.get("result.out"));

      assertEquals(lines.get(0),"anxiety=5");
      assertEquals(lines.get(lines.size()-1),"water retention=1");

    }

}