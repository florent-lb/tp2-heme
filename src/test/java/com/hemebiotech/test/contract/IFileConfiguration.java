package com.hemebiotech.test.contract;

import java.net.URISyntaxException;
import java.nio.file.Paths;

/**
 * Simple File configuration for test classes
 */
public interface IFileConfiguration
{ 
      
    default String getFilePath() throws URISyntaxException 
    {        
        
          return Paths.get("src/main/resources/fichier-symptom/symptoms.txt").toFile()
                    .getAbsolutePath();
              
    }

}