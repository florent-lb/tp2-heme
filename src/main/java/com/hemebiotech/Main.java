package com.hemebiotech;

import java.nio.file.Paths;
import java.util.Optional;

import com.hemebiotech.analytics.controller.AnalyticsController;
import com.hemebiotech.analytics.file.impl.ReaderSymptomDataFromFile;
import com.hemebiotech.analytics.file.impl.WriterSymptomDataFile;

/**
 * Main Class
 * Argument : 
 *  -path=<absolute_path_file_to_parse>
 * 
 * if path is not add, the application parse the default file in
 * ~/file-symptom/symptoms.txt
 */
public class Main
{

    public static void main(String[] args) throws Exception
    {
        // Reading input arguments
        String pathToFile = null;
        for(String arg : args)
        {
            // if a path in arguments is given
            if(arg.matches("-path=^"))
            {
                pathToFile = arg.split("=")[1];
            }
        }
        //Launching analyse
        AnalyticsController controller = new AnalyticsController();
        //Get the defaults file if no path given in arguments
         Optional.ofNullable(pathToFile)
                 .ifPresentOrElse(path -> controller.analyse(new ReaderSymptomDataFromFile(path)),
                 () -> controller.analyse(new ReaderSymptomDataFromFile(Main.class.getClassLoader().getResourceAsStream("fichier-symptom/symptoms.txt"))));
                              
       
        //Export data
        controller.exportResult(new WriterSymptomDataFile());
    }
}