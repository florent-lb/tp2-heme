package com.hemebiotech.analytics.file.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;

import static java.nio.file.StandardOpenOption.*;

import java.io.IOException;

import com.hemebiotech.analytics.entity.ReportSymtom;
import com.hemebiotech.analytics.file.contract.ISymptomWriter;

import lombok.extern.log4j.Log4j2;
/**
 * Implementation of {@link ISymptomWriter} for writting result in a simple file
 */
@Log4j2(topic ="com.hemebiotech")
public class WriterSymptomDataFile implements ISymptomWriter {

    @Override
    public void write(final Set<ReportSymtom> resultToWrite) throws IOException {

        final Path pathToFile = Paths.get("result.out");

        // Aggregate results to one String for copy in file
        final String linesToCopy = resultToWrite
        .stream()
        .map( report -> report.getSymptom().getName()+"="+report.getNbOccurrence())
        .collect(Collectors.joining("\n"));

        //Writing the file
        Files.write(pathToFile,
                    linesToCopy.getBytes(),
                    TRUNCATE_EXISTING,CREATE,WRITE);

        log.info("Writting file to : " + pathToFile.toFile().getAbsolutePath());
    }

}