package com.hemebiotech.analytics.file.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import com.hemebiotech.analytics.entity.Symptom;
import com.hemebiotech.analytics.file.contract.ISymptomReader;

import lombok.extern.log4j.Log4j2;

/**
 * Implementation of {@link ISymptomReader} for simple text file
 * Must be construct with the path of the file in constructor arguments.
 *
 */
@Log4j2(topic ="com.hemebiotech")
public class ReaderSymptomDataFromFile implements ISymptomReader {

	private String filepath;

	private InputStream fileStream;
	
	/**
	 * 
	 * @param filepath a full or partial path to file with symptom strings in it, one per line
	 */
	public ReaderSymptomDataFromFile (String filepath) {
		this.filepath = filepath;
	}

		/**
	 * 
	 * @param filepath a full or partial path to file with symptom strings in it, one per line
	 */
	public ReaderSymptomDataFromFile (InputStream fileStream) {
		this.fileStream = fileStream;
	}
	
	@Override
	public List<Symptom> getSymptoms() {
				
		createFileFromStream();
		Objects.requireNonNull(filepath,"The path of the file to read is null, please verify the instance construction of your ReadSymptomDataFromFile");
						
		try {
			/*
			 * Return all symptoms in the file and create them in new Object Symptom
			 */
			log.trace("Read file : " + filepath );
			return Files.readAllLines(Paths.get(filepath),StandardCharsets.UTF_8)
									.stream()
									.map(line -> new Symptom(line))
									.collect(Collectors.toList());
		} catch (IOException e) 
		{		
			log.error("Impossible to read the file : " + filepath);				
			return Collections.emptyList();
		}				
	
	}

	private void createFileFromStream() {
		
		if(fileStream != null)
		{
			try {
				Path dirPath = Files.createTempDirectory("heme_biotech_symptom_");
				Path filepathTemp = Paths.get(dirPath.toFile().getAbsolutePath(), "symtpom.txt");
				Files.copy(fileStream, filepathTemp , StandardCopyOption.REPLACE_EXISTING);
				filepath = filepathTemp.toFile().getAbsolutePath();
			} catch (IOException e) {
				log.error("Impossible de créer le dossier et ou fichier : " + e.getMessage());
			}
		}		
	}

	

}
