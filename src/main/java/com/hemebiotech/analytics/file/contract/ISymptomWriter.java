package com.hemebiotech.analytics.file.contract;

import java.util.Set;

import com.hemebiotech.analytics.entity.ReportSymtom;

/**
 * API for writing the result incomming from an {@link com.hemebiotech.controller.AnalyticsController} 
 */
@FunctionalInterface
public interface ISymptomWriter {

    /**
     * Method to use for writing the result with the implementation
     * @param report List of reports for each symptom to write in the ouput
     * @throws Exception If the output failed, depends on implementation
     */
    void write(Set<ReportSymtom> report) throws Exception;
	
}
