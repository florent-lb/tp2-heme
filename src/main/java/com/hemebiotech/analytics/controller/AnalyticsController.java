package com.hemebiotech.analytics.controller;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.hemebiotech.analytics.entity.ReportSymtom;
import com.hemebiotech.analytics.entity.Symptom;
import com.hemebiotech.analytics.file.contract.ISymptomReader;
import com.hemebiotech.analytics.file.contract.ISymptomWriter;

import lombok.extern.log4j.Log4j2;

/**
 * Main controller of the application Read the file given in constructor,
 * analyse it and write the result in a file
 */
@Log4j2(topic ="com.hemebiotech")
public class AnalyticsController {

    private Set<ReportSymtom> results;

    /**
     * Launched the analyse of the file, and stock the result in a Set.
     * Each analyse erase the precedent result
     */
    public void analyse(ISymptomReader reader) {
        Objects.requireNonNull(reader, "The reader is null, please check your call");
               
        List<Symptom> listSymptoms = reader.getSymptoms();
        // Analyse the file's result and auto-sorted it
        log.info("Reading symtpoms : " + listSymptoms.size() + " symptoms found");
        results = new TreeSet<>(
                listSymptoms
                .stream()      
                //Count number off occurences for each Symptom          
                .collect(Collectors.groupingBy((Symptom symptom) -> symptom, Collectors.counting()))
                .entrySet()
                .stream()
                //Create Report Symptom
                .map(entry -> new ReportSymtom(entry.getKey(), entry.getValue()))
                .collect(Collectors.toSet())
                );

        log.info("Report symtpoms : " + results.size() + " report generated");
    }

    /**
     * Export the last results to a file result.out
     */
    public void exportResult(ISymptomWriter writer) {       
        try {
            writer.write(getResults());
        } catch (Exception e) {
           log.error("Export of result failed : " + e.getMessage());
        }
    }

    /**
     * Getter for map Map<String,Long> results
     * @return An empty map if no analyse is launching 
     * This map is unmodifiable 
     */
    public Set<ReportSymtom> getResults()
    {
        if(results == null)
        {
            results = Collections.emptySet();
        }
        return Collections.unmodifiableSet(results);
    }
}