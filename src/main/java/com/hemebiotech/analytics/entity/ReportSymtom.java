package com.hemebiotech.analytics.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class ReportSymtom implements Comparable<ReportSymtom> {

    @NonNull
    private Symptom symptom;

    private Long nbOccurrence;

    @Override
    public int compareTo(ReportSymtom o) {
        
        return  getSymptom().compareTo(o.getSymptom());
    }
}