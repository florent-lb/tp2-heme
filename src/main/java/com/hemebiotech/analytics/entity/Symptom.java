package com.hemebiotech.analytics.entity;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Symptom implements Comparable<Symptom>{
    
    @NonNull
    private String name;

    @Override
    public int compareTo(Symptom o) {
      return getName().compareTo(o.getName());
    }

}