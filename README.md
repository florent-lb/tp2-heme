# Projet 2 - Heme Biotech

Projet build with Maven

For testing 
- mvn test -f ./pom.xml

For build application
- mvn clean package

Arguments (optionnal) : 
>  -path=<absolute_path_file_to_parse>

By default, the application parse the file ./file-symptom/symptoms.txt

The result will created in ./result.out

Logs are availabe in ./logs